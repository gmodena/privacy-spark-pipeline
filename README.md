[![Project Status: Concept – Minimal or no implementation has been done yet, or the repository is only intended to be a limited example, demo, or proof-of-concept.](https://www.repostatus.org/badges/latest/concept.svg)](https://www.repostatus.org/#concept)


# privacy-spark-pipeline

This repo is a work-in-progress that contains a concept demo Spark3 pipeline for 
differentially private analytics.

This pipeline performs a private count, with contribution bounding,
on `pageview_actor` and saves the summary stats to HDFS under `pageview_actor_count`.

It runs with the following default settings (`src/main/resources/application.conf`):
```
file.input = "/wmf/data/wmf/pageview/actor/year=2021/month=8/day=15/hour=6"
file.output = "pageview_actor_count"

epsilon = 0.47712125472
maxContributions = 2
maxContributionsPerPartition = 5
```

This private aggregation is inspired by [https://github.com/htried/pyspark_dp_beta](https://github.com/htried/pyspark_dp_beta).

## TODO:
* [] Document a DIY Spark3 deployment on analytics infra
* [] Make airflow dag working with our dev instances and spark3 targets.

# Private counts

Differential privacy is implemented atop Google's [differential-privacy](https://github.com/google/differential-privacy) library.
In this demo we use an experimental wrapper, 
available at [https://github.com/gmodena/spark-privacy](https://github.com/gmodena/spark-privacy), that exposes private aggregation functionality
as User Defined Aggregate Functions.

A private `count()` aggregation can be performed in SparkSQL (or the equivalent DataFrame Scala api)
with:
```bash
SELECT
  project,
  country,
  private_count(page_title),
  count(page_title) as non_private_count
FROM pageview_actor_bounded
GROUP BY project, country
```

Where `private_count` is a UDAF registerd with
```scala
import privacy.spark.{BoundContribution, PrivateCount}

val privateCount = new PrivateCount[Long](settings.epsilon, settings.maxContributions)
spark.udf.register("private_count", udaf(privateCount))
```

# Build

Project builds are managed by Gradle. Some deps are feteched from Github's maven repos that requires
and account and access token to authenticate.

```bash
export GITHUB_ACTOR=<your github user>
export GITHUB_TOKEN=<your github access token>
```

The project can be built with.
```
./gradlew build 
```

or, to generate a fat jar with all dependencies,
```
./gradlew shadowJar
```

The project jars will be available at `build/libs/privacy-spark-pipeline-1.0-SNAPSHOT.jar` and 
`build/libs/privacy-spark-pipeline-1.0-SNAPSHOT-all.jar` respectively.

## Runtime dependencies

Running this pipeline requires Spark3 and the following
runtime deps:
  * `privacy.spark:spark-privacy:1.0-SNAPSHOT` 
  * `com.google.privacy.differentialprivacy:differentialprivacy:1.1.0`



