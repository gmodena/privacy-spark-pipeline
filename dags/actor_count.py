from airflow.models import DAG
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator

args = { "start_date": days_ago(1),
         "schedule_interval": None }

with DAG(dag_id="compute_actor_count",
        tags=['differential-privacy', 'spark3']) as dag:

    spark_submit = SparkSubmitOperator(
            task_id="spark_submit",
            application="privacy-spark-pipeline-1.0-SNAPSHOT.jar",
            packages="privacy.spark:spark-privacy:1.0-SNAPSHOT,com.google.privacy.differentialprivacy:differentialprivacy:1.1.0",
            repositories="https://maven.pkg.github.com/gmodena/spark-privacy",
            dag=dag)

    spark_submit