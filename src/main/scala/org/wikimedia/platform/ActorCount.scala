package org.wikimedia.platform

import org.apache.spark.sql.SparkSession
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.sql.functions.{col, count, udaf}
import privacy.spark.{BoundContribution, PrivateCount}

object ActorCount {
  def main(args: Array[String]): Unit = {
    val conf: Config = ConfigFactory.load()
    val settings: Settings = new Settings(conf)

    val spark:SparkSession = SparkSession.builder()
      .getOrCreate()
    import spark.implicits._

    val privateCount = new PrivateCount[Long](settings.epsilon, settings.maxContributions)
    spark.udf.register("private_count", udaf(privateCount))

    spark.read.parquet(settings.inputPath)
      .createOrReplaceTempView("pageview_actor")

    spark.sql("""
      SELECT
        pageview_info['page_title'] as page_title,
        page_id,
        pageview_info['project'] as project,
        geocoded_data['country'] as country,
        actor_signature
      FROM pageview_actor
      WHERE page_id IS NOT NULL
     """)
      .transform(BoundContribution("page_id", "actor_signature", settings.maxContributions, settings.maxContributionsPerPartition))
      .createOrReplaceTempView("pageview_actor_bounded")

    val dataframe = spark.sql("""
      SELECT
            project,
            country,
            private_count(page_title) as private_count,
            count(page_title) as non_private_count
      FROM pageview_actor_bounded
      GROUP BY project, country
      """).write
      .mode("overwrite")
      .parquet(settings.outputPath)
  }
}
