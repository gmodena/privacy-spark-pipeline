package org.wikimedia.platform

import com.typesafe.config.Config
import scala.collection.JavaConverters._

class Settings(config: Config) extends Serializable {
  val inputPath = config.getString("file.input")
  val outputPath = config.getString("file.output")

  val epsilon = config.getDouble("epsilon")
  val maxContributions = config.getInt("maxContributions")
  val maxContributionsPerPartition = config.getInt("maxContributionsPerPartition")
}